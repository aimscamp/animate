package com.example.lynn.animate;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import android.widget.Button;

import static com.example.lynn.animate.MainActivity.*;

/**
 * Created by lynn on 6/16/2016.
 */
public class MyListener implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        if (v instanceof Button) {
            Button source = (Button)v;

            if (source == animate) {
                AnimatorSet set = new AnimatorSet();

                ObjectAnimator[] objectAnimators = new ObjectAnimator[5];

                objectAnimators[0] = ObjectAnimator.ofFloat(button,"translationX",200,500);
                objectAnimators[1] = ObjectAnimator.ofFloat(button,"translationY",0,200);
                objectAnimators[2] = ObjectAnimator.ofFloat(button,"scaleX",1,2);
                objectAnimators[3] = ObjectAnimator.ofFloat(button,"scaleY",1,2);
                objectAnimators[4] = ObjectAnimator.ofFloat(button,"rotation",0,500);

                set.play(objectAnimators[0]);
                set.play(objectAnimators[1]).with(objectAnimators[0]);
                set.play(objectAnimators[2]).after(objectAnimators[1]);
                set.play(objectAnimators[3]).with(objectAnimators[2]).after(objectAnimators[1]);
                set.play(objectAnimators[4]).after(objectAnimators[3]);

                set.setDuration(10000);

                set.start();
            }
        }

    }

}
